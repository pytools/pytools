# -*- coding: utf-8 -*-
from distutils.core import setup

setup(name = 'LifeTools',
      version = 'Alpha',
      description = 'Tools for an easier life',
      author = 'Vourlakis Nikolas',
      author_email = 'nvourlakis@gmail.com',
      scripts = ['scripts/rmjindent', 'scripts/cpplint']
      )
